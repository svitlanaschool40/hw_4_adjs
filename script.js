function getCharacters(film) {
  const charactersPromises = film.characters.map(function(characterUrl) {
    return fetch(characterUrl)
      .then(function(response) {
        return response.json();
      });
  });

  return Promise.all(charactersPromises);
}

let moviesContainer = document.getElementById('movies');
moviesContainer.innerHTML = '<div class="loader"></div>';

fetch('https://ajax.test-danit.com/api/swapi/films')
  .then(function(response) {
    return response.json();
  })
  .then(function(data) {
    console.log(data);
    moviesContainer.innerHTML = '';
  
    data.forEach(function(currentFilm) {
      let movieDiv = document.createElement('div');
      let filmTitle = currentFilm.title ? currentFilm.title : 'Unknown';
    
      movieDiv.innerHTML = '<h3>Episode ' + currentFilm.episodeId + ': ' + filmTitle + '</h3>' +
        '<p>' + currentFilm.openingCrawl + '</p>';
    
      moviesContainer.appendChild(movieDiv);
    
      getCharacters(currentFilm)
        .then(function(characters) {
          characters.forEach(function(character) {
            let characterDiv = document.createElement('div');
            characterDiv.textContent = character.name;
            movieDiv.appendChild(characterDiv);
          });
        })
        .catch(function(error) {
          console.log(error);
        });
    });
  })
  .catch(function(error) {
    console.log(error);
  });